# Stereotypic morality: The influence of group membership on moral foundations

The following code and materials will reproduce the experiment and results that were reported in the following publication:

Winget, J. R., & Tindale, R. S. (2019). Stereotypic morality: The influence of group membership on moral foundations. Group Processes & Intergroup Relations. https://doi.org/10.1177/1368430219866502

All authors are affiliated with the Department of Psychology at Loyola University Chicago, Chicago, IL, USA.

The OSF project page which includes supplementary materials can be found here: https://osf.io/84t9p/

If you experience any difficulties, find any inconsistencies, or have advice on how to make this document easier to interpret please contact Jeremy R. Winget at jwinget@luc.edu

Today’s modern world affords many benefits, one of which is the ability to have near-instantaneous interactions with groups and cultures other than our own. Though advantageous in many situations, one challenge for these groups is navigating what they perceive to be right and wrong in a cooperative manner despite having different modes of morality. Moral foundations theory holds groups use the same moral foundations to guide their judgments and decision making, but there has been little research on how the perception of these foundations differs within and between groups. Thus, the current study examined how moral foundations operate from a group perspective and potential outgroup moderators of moral foundations. Participants rated the extent to which various groups used moral foundations in one of two conditions. Each condition contained an ingroup and three outgroups that conformed to the quadrants of the stereotype content model. Results showed significant differences in the harm, fairness, and loyalty foundations between ingroups and outgroups. Moreover, the type of outgroup significantly influenced moral foundations scores. These findings demonstrate the importance of considering moral foundations at the group level.